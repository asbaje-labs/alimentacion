# Alimentación

> Estudio y preparación de dietas para mejorar mi salud y rendimiento personal.

## Referencias

- [Food preservation](https://en.wikipedia.org/wiki/Food_preservation)
- [Food storage](https://en.wikipedia.org/wiki/Food_storage)

## Storage & Preservation

- Papel Aluminio
- Bolsa de Congelados
- Sellador
- …

## Medidas

![Báscula de cocina digital multifunción Etekcity, báscula de alimentos, 11 lbs, plata, acero inoxidable (pilas incluidas)](https://m.media-amazon.com/images/I/81WdcBpd9aL._SL500_.jpg)
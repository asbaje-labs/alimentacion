# Conservación de los Alimentos

> ...

![ComSaf Juego de 6 recipientes herméticos de vidrio con tapas, tarro redondo de almacenamiento de alimentos de 17 onzas, recipiente de almacenamiento con sello de preservación transparente, cierre de](https://m.media-amazon.com/images/I/817sWyeunCS._AC_SL500_.jpg)

![KAMOTA Tarros Mason de 16 onzas con tapas y bandas regulares, ideales para preparación de comidas, mermelada, miel, recuerdos de boda, regalos de ducha, alimentos para bebés, tarros de especias de](https://m.media-amazon.com/images/I/51rKNzy5AkL._AC_.jpg)

## Referencias

- [Conservación de los alimentos](https://es.wikipedia.org/wiki/Conservaci%C3%B3n_de_los_alimentos)
- [Food Preservation](https://en.wikipedia.org/wiki/Food_preservation)
- [Conservación de Quesos](https://www.youtube.com/shorts/uIUHz-ZZUek)
- Piña
- Repollo
- Lechuga
- Tomates
- Verduras
    - Ajies
    - Puerros
- ...